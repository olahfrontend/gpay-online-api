<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiSekaliPakaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_sekali_pakais', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_user");
            $table->bigInteger("id_invoice");
            $table->string("nomorinvoice");
            $table->string("nominal");
            $table->string("cvv");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_sekali_pakais');
    }
}
