<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("setting","UserController@getSetting");
Route::post("setting/update","UserController@updateSetting");

Route::get("user", "UserController@index");
Route::get("user/{id}", "UserController@getPrivillege");
Route::get("marketing", "UserController@getMarketing");
Route::post('user/register', 'UserController@create');
Route::post('user/login', 'UserController@login');
Route::post('user/updateprivillege', 'UserController@updatePrivillege');
Route::post("user/forgotpass", "UserController@forgotPassword");
Route::post("user/verifpass", "UserController@checkVerification");
Route::post("user/changepass","UserController@changePassword");

Route::post('rekening/create/{id}', 'BankController@create');
Route::get('rekening/{id}', 'BankController@index');

Route::get('listbank', 'ListBankController@index');
Route::post("listbank/update", 'ListBankController@update');
Route::post("listbank/add", 'ListBankController@add');
Route::delete("listbank/delete/{id}", 'ListBankController@delete');

Route::get('kartu/{id}', 'KartuController@index');
Route::post('kartu/create', 'KartuController@create');
Route::post("kartu/cvv", "TransaksiController@getCVV");

Route::get("transaksi", "TransaksiController@getAllTransaksi");
Route::get("komisi", "TransaksiController@getMyKomisi");
Route::get("history", "TransaksiController@getHistory");
Route::get("transaksi/detail/{id}", "TransaksiController@getDetailTransaksi");
Route::post("transaksi/updatestatus", "TransaksiController@updateStatus");
Route::post("updatestatus", "TransaksiController@updateStatus");
Route::get("updatestatus", "TransaksiController@updateStatus");
Route::post("transaksi/addotp", "TransaksiController@addOTP");
Route::get("transaksi/{id}", "TransaksiController@getStatusTransaksi");
Route::post("transaksi/delete", "TransaksiController@deleteTransaksiSekaliPakai");
Route::post("transaksi/uploadbukti", "TransaksiController@uploadBukti");
Route::post("transaksi/uploadkomisi", "TransaksiController@uploadBuktiKomisi");

Route::get("fee", "FeeController@index");
Route::post("fee/update", "FeeController@update");
Route::post("fee/updatekomisi", "FeeController@updatekomisi");
Route::get("fee/detail", "FeeController@getDetail");
Route::get("fee/komisi", "FeeController@getKomisi");
