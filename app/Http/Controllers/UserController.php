<?php

namespace App\Http\Controllers;

use App\forgot_password;
use App\Komisi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    //
    public function index()
    {
        $resp["statusCode"] = 200;
        $resp["data"] = User::all();
        return  response($resp, 200);
    }

    public function updatePrivillege(request $request)
    {

        if ($request->id != null && $request->privilege != null && $request->status != null) {
            if (DB::table('users')
                ->where('id', $request->id)
                ->update(['privilege' => $request->privilege, 'status' => $request->status])
            ) {
                if ($request->privilege == 2) {
                    $dt = DB::table("komisis")->where('id', $request->id)->get();
                    if (count($dt) <= 0) {
                        $data = new Komisi;
                        $data->id_user = $request->id;
                        $data->paket6 = 1;
                        $data->paket12 = 1;
                        $data->paket18 = 1;
                        $data->paket24 = 1;
                        $data->paket36 = 1;
                        $data->save();
                    }
                }
                $resp["statusCode"] = 200;
                $resp["message"] = "Success Update Privillege";
                return  response($resp, 200);
            }
            $resp["statusCode"] = 400;
            $resp["message"] = "Update failed";
            return  response($resp, 200);
        }

        $resp["statusCode"] = 400;
        $resp["message"] = "Parameter tidak lengkap";
        return  response($resp, 200);
    }

    public function create(request $request)
    {
        try {
            $data = new User;
            $data->email = $request->email;
            $data->name = $request->nama;
            $data->phone = $request->phone;
            $data->password = Hash::make($request->password, [
                'rounds' => 12
            ]);

            if ($data->email != null && $data->name != null && $data->phone != null && $data->password != null) {
                if ($request->privilege != null) {
                    $data->privilege = $request->privilege;
                }
                if ($data->save()) {
                    $resp["statusCode"] = 200;
                    $resp["message"] = "Register Berhasil";
                    return  response($resp, 200);
                }
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "Parameter Tidak Lengkap";
                return  response($resp, 400);
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                $resp["statusCode"] = 203;
                $resp["message"] = "Email sudah ada";
                return  response($resp, 203);
            }
        }
    }

    public function getPrivillege($id)
    {
        try {
            $data = User::select("privilege")->where('id', $id)->first();
            if ($data != null) {
                $resp["statusCode"] = 200;
                $resp["data"] = $data;
                return  response($resp, 200);
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "Data not found";
                return  response($resp, 400);
            }
        } catch (QueryException $e) {
            $resp["statusCode"] = 500;
            $resp["message"] = $e;
            return response($resp, 500);
        }
    }

    public function login(request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        // echo date("H");
        $jamskrg = date("H");
        $jambuka = 0;
        $jamtutup = 0;
        $data = DB::table("settings")
            ->get();
        for ($i = 0; $i < count($data); $i++) {
            // echo $data[$i]->nama_setting;
            if ($data[$i]->nama_setting == "JAM_BUKA") {
                $jambuka = $data[$i]->value;
            } else if ($data[$i]->nama_setting == "JAM_TUTUP") {
                $jamtutup = $data[$i]->value;
            }
        }

        if ($jamskrg >= $jambuka && $jamskrg < $jamtutup) {
            $email = $request->email;
            $password = $request->password;

            $user = DB::table('users')->where('email', $email)->first();
            if ($user != null) {
                if (Hash::check($password, $user->password)) {
                    if ($user->status == "1") {
                        $resp["statusCode"] = 200;
                        $resp["data"] = $user;
                        return  response($resp, 200);
                    } else if ($user->status == "0") {
                        $resp["statusCode"] = 201;
                        $resp["message"] = "Akun masih pending, harap tunggu verifikasi dari admin.";
                        return  response($resp, 201);
                    } else {
                        $resp["statusCode"] = 201;
                        $resp["message"] = "Akun anda telah dibekukan karena telah melanggar aturan dari kami.\n Silahkan hubungi admin untuk info lebih lanjut.";
                        return  response($resp, 201);
                    }
                } else {
                    $resp["statusCode"] = 203;
                    $resp["message"] = "Password Salah";
                    return  response($resp, 203);
                }
            } else {
                $resp["statusCode"] = 203;
                $resp["message"] = "Email tidak terdaftar";
                return  response($resp, 203);
            }
        } else {
            $resp["statusCode"] = 203;
            $resp["message"] = "Silahkan login pada jam operasional.\n Jam operasional : " . $jambuka . ":00 - " . $jamtutup . ":00";
            return  response($resp, 203);
        }
    }

    public function update(request $request, $id)
    {
        $nama = $request->nama;
        $alamat = $request->alamat;

        $data = User::find($id);
        $data->nama = $nama;
        $data->alamat = $alamat;
        $data->save();

        return "Data berhasil diupdate";
    }

    public function delete($id)
    {
        $data = User::find($id);
        $data->delete();

        return "Data berhasil didelete";
    }

    public function forgotPassword(request $request)
    {
        try {
            $data = User::select('name')->where("email", $request->email)->first();
            if ($data != null) {
                $nama = $data->name;
                $pesan = $this->generateRandomString(6);
                Mail::send('email', ['nama' => $nama, 'pesan' => $pesan], function ($message) use ($request) {
                    $message->subject("Forgot Password GpayOnline");
                    $message->from('admin@gpayonline.com', 'Gpay');
                    $message->to($request->email);
                });

                $data = new forgot_password;
                $data->email = $request->email;
                $data->code = $pesan;
                $data->save();
                $resp["statusCode"] = 200;
                $resp["message"] = "Email Terkirim";
                return  response($resp, 200);
            }
            $resp["statusCode"] = 400;
            $resp["message"] = "Email tidak terdaftar";
            return  response($resp, 400);
        } catch (Exception $e) {
            $resp["statusCode"] = 500;
            $resp["message"] = $e->getMessage();
            return  response($resp, 500);
        }
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function checkVerification(request $request)
    {
        $data = DB::table("forgot_passwords")->where('email', $request->email)->orderByDesc("id")->first();

        if ($data) {
            if ($request->code == $data->code) {
                $resp["statusCode"] = 200;
                $resp["message"] = "Success";
                return  response($resp, 200);
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "Kode Salah";
                return  response($resp, 400);
            }
        } else {
            $resp["statusCode"] = 400;
            $resp["message"] = "Kode tidak exist";
            return  response($resp, 400);
        }
    }

    function changePassword(request $request)
    {
        $password = Hash::make($request->password, [
            'rounds' => 12
        ]);

        if (DB::table('users')
            ->where('email', $request->email)
            ->update(['password' => $password])
        ) {
            $resp["statusCode"] = 200;
            $resp["message"] = "Success";
            return  response($resp, 200);
        } else {
            $resp["statusCode"] = 400;
            $resp["message"] = "Ganti Password gagal";
            return  response($resp, 400);
        }
    }

    function getMarketing()
    {
        try {
            $data = DB::table("komisis")
                ->join("users", "users.id", "=", "komisis.id_user")
                ->get(array("komisis.*", "users.name", "users.email"));
            $resp["statusCode"] = 200;
            $resp["data"] = $data;
            return  response($resp, 200);
        } catch (QueryException $e) {
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return  response($resp, 400);
        }
    }

    function getSetting()
    {
        try {
            $data = DB::table("settings")
                ->get();
            $resp["statusCode"] = 200;
            $resp["data"] = $data;
            return  response($resp, 200);
        } catch (QueryException $e) {
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return  response($resp, 400);
        }
    }

    function updateSetting(request $request)
    {
        $jambuka = $request->jambuka;
        $jamtutup = $request->jamtutup;

        DB::table("settings")->where("nama_setting", 'JAM_BUKA')->update(['value' => $jambuka]);
        DB::table("settings")->where("nama_setting", 'JAM_TUTUP')->update(['value' => $jamtutup]);

        $resp["statusCode"] = 200;
        $resp["message"] = "Success";
        return  response($resp, 200);
    }
}
