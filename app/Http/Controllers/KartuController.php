<?php

namespace App\Http\Controllers;

use App\Kartu;
use App\transaksi;
use App\transaksi_sekali_pakai;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KartuController extends Controller
{
    //
    public function index($id)
    {
        try {
            $data = DB::table("kartus")->where("id_user", $id)->get();
            if ($data != null) {
                $resp["statusCode"] = 200;
                $resp["data"] = $data;
                return response($resp, 200);
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "data not found";
                return response($resp, 400);
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $errorCode;
            return response($resp, 400);
        }
    }

    public function create(request $request)
    {
        try {
            if (
                $request->id_user != null && $request->nominal != null && $request->cvv != null
                && $request->nomor != null && $request->nama != null && $request->expired != null
                && $request->cicilan != null && $request->nominalawal != null && $request->namabank != null
            ) {
                //SIMPAN KARTU
                $found = DB::table('kartus')->where([
                    ['id_user', '=', $request->id_user],
                    ['nomor', '=', $request->nomor],
                ])->first();
                if ($found != null) {
                    $data = Kartu::find($found->id);
                } else {
                    $data = new Kartu();
                }
                $data->id_user = $request->id_user;
                $data->nomor = $request->nomor;
                $data->nama = $request->nama;
                $data->expired = $request->expired;
                if ($data->save()) {
                    //SIMPAN TRANSAKSI
                    $idkartu = DB::table("kartus")->max("id");
                    if ($found != null) {
                        $idkartu = $found->id;
                    }
                    $noinv = "INV" . date("dmyHis");
                    $data = new transaksi();
                    $data->id_user = $request->id_user;
                    $data->nominal = $request->nominal;
                    $data->namabankkartu = $request->namabank;
                    $data->nomorinvoice = $noinv;
                    $data->id_kartu = $idkartu;
                    $data->status = 0;
                    $data->nominalawal = $request->nominalawal;
                    $data->cicilan = $request->cicilan;
                    $data->hargaproduk = $request->hargaproduk;
                    // $data->komisi = 0;
                    if ($request->komisi != null) {
                        $data->komisi = $request->komisi;
                    }
                    if ($data->save()) {
                        //SIMPAN TRANSAKSI SEKALI PAKAI
                        $idinvoice = DB::table("transaksis")->max("id");
                        $data = new transaksi_sekali_pakai();
                        $data->id_user = $request->id_user;
                        $data->nomorinvoice = $noinv;
                        $data->nominal = $request->nominal;
                        $data->id_invoice = $idinvoice;
                        $data->cvv = $request->cvv;

                        if ($data->save()) {
                            $resp["statusCode"] = 200;
                            $resp["message"] = "Transaksi berhasil dibuat";
                            return  response($resp, 200);
                        } else {
                            $resp["statusCode"] = 401;
                            $resp["message"] = "Simpan transaksi sekali pakai gagal";
                            return  response($resp, 401);
                        }
                    } else {
                        $resp["statusCode"] = 401;
                        $resp["message"] = "Simpan Transaksi gagal";
                        return  response($resp, 401);
                    }
                } else {
                    $resp["statusCode"] = 401;
                    $resp["message"] = "Simpan Kartu gagal";
                    return  response($resp, 401);
                }
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "Parameter Tidak Lengkap";
                return  response($resp, 400);
            }
        } catch (QueryException $e) {
            // $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return  response($resp, 400);
        }
    }
}
