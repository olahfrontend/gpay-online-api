<?php

namespace App\Http\Controllers;

use App\transaksi;
use App\transaksi_sekali_pakai;
use Facade\Ignition\QueryRecorder\Query;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    //

    public function addOTP(request $request)
    {
        if ($request->id_user != null && $request->otp != null && $request->status) {
            $data = DB::table("transaksi_sekali_pakais")->where('id_user', $request->id_user)->orderBy('id', 'desc')->first();
            if ($data != null) {
                //status = 1 -> otp transaksi
                if ($request->status == 1) {
                    if (DB::table('transaksis')
                        ->where('id', $data->id_invoice)
                        ->update(['otp' => $request->otp, 'status' => "2"])
                    ) {
                        $resp["statusCode"] = 200;
                        $resp["message"] = "OTP telah ditambahkan";
                        return response($resp, 200);
                    } else {
                        $resp["statusCode"] = 400;
                        $resp["message"] = "Gagal Update";
                        return response($resp, 400);
                    }
                }
                //status = 2 -> otp kartu
                else if ($request->status == 2) {
                    if (DB::table('transaksis')
                        ->where('id', $data->id_invoice)
                        ->update(['otp' => $request->otp, 'status' => "7"])
                    ) {
                        $resp["statusCode"] = 200;
                        $resp["message"] = "OTP telah ditambahkan";
                        return response($resp, 200);
                    } else {
                        $resp["statusCode"] = 400;
                        $resp["message"] = "Gagal Update";
                        return response($resp, 400);
                    }
                }
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "Transaksi tidak ditemukan";
                return response($resp, 400);
            }
        } else {
            $resp["statusCode"] = 400;
            $resp["message"] = "Parameter tidak lengkap";
            return response($resp, 400);
        }
    }

    public function updateStatus(request $request)
    {
        if ($request->no_inv != null && $request->status != null) {
            try {
                $nomorinvoice = $request->no_inv;
                $status = $request->status;

                if ($status == -1) {
                    $note = $request->note;
                    if (DB::table('transaksis')
                        ->where('nomorinvoice', $nomorinvoice)
                        ->update(['status' => $status, 'note' => $note])
                    ) {
                        $resp["statusCode"] = 200;
                        $resp["message"] = "Status updated";
                        return response($resp, 200);
                    } else {
                        $resp["statusCode"] = 400;
                        $resp["message"] = "Invoice not exist";
                        return response($resp, 400);
                    }
                } else if ($status == 5) {
                    if (DB::table('transaksis')
                        ->where('nomorinvoice', $nomorinvoice)
                        ->update(['status' => $status])
                    ) {
                        if (DB::table('transaksi_sekali_pakais')->where('nomorinvoice', $nomorinvoice)->delete()) {
                            $resp["statusCode"] = 200;
                            $resp["message"] = "Transaksi Selesai";
                            return response($resp, 200);
                        } else {
                            $resp["statusCode"] = 400;
                            $resp["message"] = "Invoice tidak valid";
                            return response($resp, 400);
                        }
                    } else {
                        $resp["statusCode"] = 400;
                        $resp["message"] = "Invoice not exist";
                        return response($resp, 400);
                    }
                } else {
                    if (DB::table('transaksis')
                        ->where('nomorinvoice', $nomorinvoice)
                        ->update(['status' => $status])
                    ) {
                        $resp["statusCode"] = 200;
                        $resp["message"] = "Status updated";
                        return response($resp, 200);
                    } else {
                        $resp["statusCode"] = 400;
                        $resp["message"] = "Invoice not exist";
                        return response($resp, 400);
                    }
                }
            } catch (QueryException $e) {
                $errorCode = $e->errorInfo[1];
                $resp["statusCode"] = 400;
                $resp["message"] = $errorCode;
                return response($resp, 400);
            }
        }
    }


    public function getStatusTransaksi($iduser)
    {
        try {
            $data = DB::table("transaksi_sekali_pakais")->where('id_user', $iduser)->orderBy('id', 'desc')->first();
            if ($data != null) {
                $dt = DB::table("transaksis")->where("id", $data->id_invoice)->first();
                if ($dt->status == 0) {
                    $resp["statusCode"] = 200;
                    $resp["data"] = $dt;
                    $resp["message"] = "Tunggu Konfirmasi";
                    return response($resp, 200);
                } else if ($dt->status == 1) {
                    $resp["statusCode"] = 201;
                    $resp["data"] = $dt;
                    $resp["message"] = "Data telah dikonfirmasi";
                    return response($resp, 201);
                } else if ($dt->status == 2) {
                    $resp["statusCode"] = 200;
                    $resp["data"] = $dt;
                    $resp["message"] = "Tunggu Konfirmasi OTP Transaksi";
                    return response($resp, 200);
                } else if ($dt->status == 6) {
                    $resp["statusCode"] = 201;
                    $resp["data"] = $dt;
                    $resp["message"] = "Konfirmasi OTP Kartu Kredit";
                    return response($resp, 201);
                } else if ($dt->status == 7) {
                    $resp["statusCode"] = 200;
                    $resp["data"] = $dt;
                    $resp["message"] = "Tunggu Konfirmasi OTP Kartu Kredit";
                    return response($resp, 200);
                } else if ($dt->status == -1) {
                    $resp["statusCode"] = 401;
                    $resp["data"] = $dt;
                    $resp["message"] = "Data ditolak";
                    return response($resp, 401);
                } else if ($dt->status == 3) {
                    $resp["statusCode"] = 202;
                    $resp["data"] = $dt;
                    $resp["message"] = "Data OTP telah dikonfirmasi";
                    return response($resp, 202);
                } else if ($dt->status == 4) {
                    $resp["statusCode"] = 203;
                    $resp["data"] = $dt;
                    $resp["message"] = "Bukti Transfer telah dikirim";
                    return response($resp, 203);
                }
            } else {
                $resp["statusCode"] = 404;
                $resp["message"] = "Tidak memiliki transaksi aktif";
                return response($resp, 404);
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $errorCode;
            return response($resp, 400);
        }
    }

    public function deleteTransaksiSekaliPakai(request $request)
    {
        try {
            $noinv = $request->noinv;
            if ($noinv != null) {
                if (DB::table('transaksi_sekali_pakais')->where('nomorinvoice', $noinv)->delete()) {
                    $resp["statusCode"] = 200;
                    $resp["message"] = "Success Delete";
                    return response($resp, 200);
                } else {
                    $resp["statusCode"] = 404;
                    $resp["message"] = "Invoice tidak valid";
                    return response($resp, 404);
                }
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "Parameter tidak lengkap";
                return response($resp, 400);
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 500;
            $resp["message"] = $errorCode;
            return response($resp, 500);
        }
    }


    public function getAllTransaksi()
    {
        $resp["statusCode"] = 200;
        $resp["data"] = transaksi::select("id", "nomorinvoice", "status", "nominal")->orderBy('id', 'desc')->get();
        return response($resp, 200);
    }

    public function getDetailTransaksi($id)
    {
        $datatransaksi = DB::table("transaksis")->where('id', $id)->first();
        $id_user = $datatransaksi->id_user;
        $id_kartu = $datatransaksi->id_kartu;
        $datakartu = DB::table("kartus")->where('id', $id_kartu)->first();
        $databank = DB::table("banks")->where('id_user', $id_user)->first();
        $datauser = DB::table("users")->where('id', $id_user)->first();

        $resp["statusCode"] = 200;
        $resp["kartu"] = $datakartu;
        $resp["bank"] = $databank;
        $resp["transaksi"] = $datatransaksi;
        $resp["user"] = $datauser;
        return response($resp, 200);
    }

    public function getCVV(request $request)
    {
        try {
            $idinvoice = $request->id_invoice;
            $data = transaksi_sekali_pakai::select("cvv")->where('id_invoice', $idinvoice)->first();
            if ($data != null) {
                $resp["statusCode"] = 200;
                $resp["data"] = $data;
                return response($resp, 200);
            } else {
                $resp["statusCode"] = 404;
                $resp["message"] = "data not found";
                return response($resp, 404);
            }
        } catch (QueryException $e) {
            $resp["statusCode"] = 500;
            $resp["message"] = $e;
            return response($resp, 500);
        }
    }

    public function uploadBukti(request $request)
    {
        try {
            $bukti = $request->bukti;
            $idinvoice = $request->id_invoice;
            if (DB::table('transaksis')
                ->where('id', $idinvoice)
                ->update(['buktitransfer' => $bukti, 'status' => 4])
            ) {
                $resp["statusCode"] = 200;
                $resp["message"] = "Success upload bukti transfer";
                return response($resp, 200);
            } else {
                $resp["statusCode"] = 404;
                $resp["message"] = "Invoice tidak valid";
                return response($resp, 404);
            }
        } catch (QueryException $e) {
            $resp["statusCode"] = 500;
            $resp["message"] = $e;
            return response($resp, 500);
        }
    }


    public function uploadBuktiKomisi(request $request)
    {
        try {
            $buktikomisi = $request->buktikomisi;
            $idinvoice = $request->id_invoice;
            if (DB::table('transaksis')
                ->where('id', $idinvoice)
                ->update(['buktikomisi' => $buktikomisi])
            ) {
                $resp["statusCode"] = 200;
                $resp["message"] = "Success upload bukti transfer";
                return response($resp, 200);
            } else {
                $resp["statusCode"] = 404;
                $resp["message"] = "Invoice tidak valid";
                return response($resp, 404);
            }
        } catch (QueryException $e) {
            $resp["statusCode"] = 500;
            $resp["message"] = $e;
            return response($resp, 500);
        }
    }

    public function getHistory(request $request)
    {
        try {
            if ($request->id != null) {
                $data = DB::table('transaksis')
                    ->select("id", "nomorinvoice", "nominal", "status", "cicilan", "created_at")
                    ->where('id_user', $request->id)
                    ->orderByDesc("id")
                    ->get();
                $resp["statusCode"] = 200;
                $resp["data"] = $data;
                return response($resp, 200);
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "Param tidak lengkap";
                return response($resp, 400);
            }
        } catch (QueryException $e) {
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return response($resp, 400);
        }
    }

    public function getMyKomisi(request $request)
    {
        try {
            if ($request->id != null) {
                $data = DB::table('transaksis')
                    ->select("id", "nomorinvoice", "nominal", "status", "created_at", "komisi", "buktikomisi")
                    ->where('id_user', $request->id)
                    ->where('komisi', '!=', null)
                    ->where('status', '=', 5)
                    ->orderByDesc("id")
                    ->get();
                $resp["statusCode"] = 200;
                $resp["data"] = $data;
                return response($resp, 200);
            }
        } catch (QueryException $e) {
        }
    }
}
