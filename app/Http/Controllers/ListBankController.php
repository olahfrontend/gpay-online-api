<?php

namespace App\Http\Controllers;

use App\list_bank;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ListBankController extends Controller
{
    //
    public function index()
    {
        try {
            $resp["statusCode"] = 200;
            $resp["data"] = list_bank::all();
            return response($resp, 200);
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $errorCode;
            return response($resp, 400);
        }
    }

    public function update(request $request)
    {
        try {
            if (DB::table('list_banks')
                ->where('id', $request->id)
                ->update([
                    'nama_bank' => $request->nama,
                    'paket6' => $request->paket6,
                    'paket12' => $request->paket12,
                    'paket18' => $request->paket18,
                    'paket24' => $request->paket24,
                    'paket36' => $request->paket36
                ])
            ) {
                $resp["statusCode"] = 200;
                $resp["message"] = "Success Update";
                return response($resp, 200);
            }
            $resp["statusCode"] = 400;
            $resp["message"] = "No data found";
            return response($resp, 400);
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $errorCode;
            return response($resp, 400);
        }
    }

    public function add(request $request)
    {
        try {
            $data = new list_bank();
            $data->nama_bank = $request->nama;
            if ($data->save()) {
                $resp["statusCode"] = 200;
                $resp["message"] = "Success insert data";
                return response($resp, 200);
            }
            $resp["statusCode"] = 400;
            $resp["message"] = "Failed insert data";
            return response($resp, 400);
        } catch (QueryException $e) {
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return response($resp, 400);
        }
    }

    public function delete($id)
    {
        try {
            $data = list_bank::find($id);
            $data->delete();
            $resp["statusCode"] = 200;
            $resp["message"] = "Success Delete";
            return response($resp, 200);
        } catch (QueryException $e) {
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return response($resp, 400);
        }
    }
}
