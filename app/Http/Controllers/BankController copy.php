<?php

namespace App\Http\Controllers;

use App\Bank;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BankController extends Controller
{
    //

    public function create(request $request, $id)
    {
        try {
            $data = new Bank();
            $data->id_user = $id;
            $data->nama = $request->nama;
            $data->nama_bank = $request->nama_bank;
            $data->nomor_rekening = $request->nomor_rekening;

            $found = DB::table('banks')->where('id_user', $id)->first();
            if ($found != null) {
                $rekening = Bank::find($found->id);
                $rekening->nama = $data->nama;
                $rekening->nama_bank = $data->nama_bank;
                $rekening->nomor_rekening = $data->nomor_rekening;
                $rekening->save();
                $resp["statusCode"] = 200;
                $resp["message"] = "Berhasil Update";
                return response($resp, 200);
            } else {
                if ($data->id_user != null && $data->nama != null && $data->nomor_rekening != null && $data->nama_bank != null) {
                    if ($data->save()) {
                        $resp["statusCode"] = 200;
                        $resp["message"] = "Simpan Rekening Berhasil";
                        return  response($resp, 200);
                    }
                } else {
                    $resp["statusCode"] = 400;
                    $resp["message"] = "Parameter Tidak Lengkap";
                    return  response($resp, 400);
                }
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $esp["message"] = $errorCode;
            return response($resp, 400);
        }
    }

    public function index($id)
    {
        try {
            $data = DB::table('banks')->where('id_user', $id)->first();
            if ($data != null) {
                $resp["statusCode"] = 200;
                $resp["data"] = $data;
                return response($resp, 200);
            } else {
                $resp["statusCode"] = 404;
                $resp["message"] = "Data Not Found";
                return response($resp, 404);
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $errorCode;
            return response($resp, 400);
        }
    }
}
