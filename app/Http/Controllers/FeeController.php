<?php

namespace App\Http\Controllers;

use App\fee;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FeeController extends Controller
{
    //
    public function index()
    {
        try {
            $data = fee::All();
            if ($data != null) {
                $resp["statusCode"] = 200;
                $resp["data"] = $data;
                return response($resp, 200);
            } else {
                $resp["statusCode"] = 400;
                $resp["message"] = "data not found";
                return response($resp, 400);
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $errorCode;
            return response($resp, 400);
        }
    }

    public function getDetail(request $request)
    {
        if ($request->privilege != null && $request->iduser != null && $request->idbank) {
            $databank = DB::table("list_banks")->where('id', $request->idbank)->get();
            if ($request->privilege == 2) {
                $data = DB::table('fees')->where('privilege', $request->privilege)
                    ->orWhere('privilege', '-1')
                    ->get();
                $datakomisi = DB::table('komisis')->where('id_user', $request->iduser)
                    ->get();
                if (count($datakomisi) <= 0) {
                    $datakomisi["paket6"] = 1;
                    $datakomisi["paket12"] = 1;
                    $datakomisi["paket18"] = 1;
                    $datakomisi["paket24"] = 1;
                    $datakomisi["paket36"] = 1;
                }
                $resp["datakomisi"] = $datakomisi;
            } else {
                $data = DB::table('fees')->where('privilege', $request->privilege)->get();
            }
            $resp["statusCode"] = 200;
            $resp["data"] = $data;
            $resp["databank"] = $databank;
            return  response($resp, 200);
        }
        $resp["statusCode"] = 400;
        $resp["message"] = "param tidak lengkap";
        return  response($resp, 400);
    }

    public function update(request $request)
    {
        try {
            if (
                $request->id != null && $request->total != null && $request->charge != null
                && $request->admin != null && $request->transaksi != null
            ) {
                if (DB::table('fees')
                    ->where('id', $request->id)
                    ->update([
                        'total_persen' => $request->total,
                        'charge_fee' => $request->charge,
                        'admin_fee' => $request->admin,
                        'transaksi_fee' => $request->transaksi
                    ])
                ) {
                    $resp["statusCode"] = 200;
                    $resp["message"] = "Success Update";
                    return  response($resp, 200);
                }
                $resp["statusCode"] = 400;
                $resp["message"] = "data not found";
                return  response($resp, 400);
            }
        } catch (QueryException $e) {
            // $errorCode = $e->errorInfo[1];
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return  response($resp, 400);
        }
    }

    public function getKomisi(request $request)
    {
        try {
            if ($request->iduser != null) {
                $data = DB::table('komisis')->where('id_user', $request->iduser)
                    ->get();
                if (count($data) > 0) {
                    $resp["statusCode"] = 200;
                    $resp["message"] = $data;
                    return  response($resp, 200);
                }
                $resp["statusCode"] = 404;
                $resp["message"] = "No data found";
                return  response($resp, 404);
            }
        } catch (QueryException $e) {
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return  response($resp, 400);
        }
    }

    public function updatekomisi(request $request)
    {
        try {
            if (DB::table('komisis')
                ->where('id', $request->id)
                ->update([
                    'paket6' => $request->paket6,
                    'paket12' => $request->paket12,
                    'paket18' => $request->paket18,
                    'paket24' => $request->paket24,
                    'paket36' => $request->paket36
                ])
            ) {
                $resp["statusCode"] = 200;
                $resp["message"] = "Sukses Update";
                return  response($resp, 200);
            }
            $resp["statusCode"] = 400;
            $resp["message"] = "Gagal Update";
            return  response($resp, 400);
        } catch (QueryException $e) {
            $resp["statusCode"] = 400;
            $resp["message"] = $e;
            return  response($resp, 400);
        }
    }
}
